﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class ThreadingTest : MonoBehaviour
{
    public string message;
    public int counter;

    Thread newThread;
    Thread newThread2;
    Thread terrainThread;

    Texture2D tex;
    public int width;
    public int height;
    public Renderer TargetRenderer;
    public float scale = 0.01f;
    private Color32[] colours;

    public int mapWidth;
    public int mapHeight;
    public float[,] heights;
    public int size;
    public Terrain terrain;
    float terrainNoise;
    public float terrainScale = 0.005f;
    [Range(0, 1)]
    public float terrainGroundLimit;
    public float seed;

    // Use this for initialization
    void Start()
    {
        colours = new Color32[width * height];
        heights = new float[mapWidth, mapHeight];
        CreateTerrain();

        tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        TargetRenderer.material.mainTexture = tex;

        newThread = new Thread(GenerateNoise);
        newThread.Start();
        newThread2 = new Thread(AnotherLongFunction);
        newThread2.Start();
    }

    // Update is called once per frame
    void Update()
    {
        seed = Time.timeSinceLevelLoad * 4;
        if (newThread != null)
        {
            if (!newThread.IsAlive)
            {
                tex.SetPixels32(colours);
                tex.Apply();

                scale = Random.Range(0.05f, 0.35f);

                newThread = new Thread(GenerateNoise);
                newThread.Start();
            }
        }
        //if (Input.GetKeyDown(KeyCode.T))
        //{
            if (terrainThread != null)
            {
                if (!terrainThread.IsAlive)
                {
                    terrain.terrainData.SetHeights(0, 0, heights);
                    //terrainScale = Random.Range(0.01f, 0.0095f);

                    terrainThread = new Thread(ChangeTerrain);
                    terrainThread.Start();
                }
            }
        //}
    }

    void GenerateNoise()
    {
        print("Generating Noise");
        for (int x = 0; x < width; x++)
        {
            float perlinNoise;
            for (int y = 0; y < height; y++)
            {
                perlinNoise = Mathf.PerlinNoise(x * scale, y * scale) * 255;
                //tex.SetPixel(x, y, new Color(perlinNoise, perlinNoise, perlinNoise));
                colours[x + y * width].b = (byte)perlinNoise;
            }
        }
        Thread.Sleep(500);
        //newThread.Abort();
    }

    void CreateTerrain()
    {
        TerrainData terrainData = new TerrainData();
        terrainData.heightmapResolution = size;
        //terrainData.alphamapResolution = size;
        terrainData.size = new Vector3(size, size, size);
        GameObject terrainGameObject = Terrain.CreateTerrainGameObject(terrainData);
        terrain = terrainGameObject.GetComponent<Terrain>();
        //terrainGameObject.GetComponent<Collider>().enabled = false; 
        terrainThread = new Thread(ChangeTerrain);
    }

    void ChangeTerrain()
    {
        for (int x = 0; x < mapWidth; x++)
        {
            for (int y = 0; y < mapHeight; y++)
            {
                terrainNoise = Mathf.PerlinNoise(seed + x * terrainScale, seed + y * terrainScale);
                if (terrainNoise > terrainGroundLimit)
                {
                    heights[x, y] = terrainNoise;
                }
                else
                {
                    heights[x, y] = terrainGroundLimit;
                }
            }
        }
        //Thread.Sleep(500);
        print("Terrain changed");
    }

    void LongFunction()
    {
        print("thread begin");
        //message = "Yes";
        //for (int i = 0; i < 20; i++)
        //{
        //    //counter = i;
        //    //print(counter);
        //}
        Thread.Sleep(2000);
        print("thread end");
    }

    void AnotherLongFunction()
    {
        //print("Another thread begin");
        //message = "No";
        //for (int i = 0; i < 20; i++)
        //{
        //    counter = i;
        //    print(counter);
        //}
        ////Thread.Sleep(4000);
        //print("Another thread end");
    }

    void LongFunctionTheThird()
    {
        //print("Third thread begin");
        //message = "Maybe, I don't know";
        ////Thread.Sleep(3000);
        //for (int i = 0; i < 20; i++)
        //{
        //    counter = i;
        //    print(counter);
        //}
        //print("Third thread end");
    }

    void FourthLongFunction()
    {
        //print("4th thread begin");
        //message = "Can you repeat that question?";
        ////Thread.Sleep(2500);
        //for (int i = 0; i < 20; i++)
        //{
        //    counter = i;
        //    print(counter);
        //}
        //print("Shhh, I'm sleeping");
        ////Thread.Sleep(2500);
        //print("4th thread end");
    }
}
